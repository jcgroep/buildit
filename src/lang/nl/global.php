<?php

return [
    'requiredFieldsExplanation' => 'Velden met een<span style="color: #ff0000"> * </span>zijn verplicht. ',
    'otherNamed' => 'Anders, namelijk: ',

    'Cancel' => 'Annuleren',
    'Close' => 'Sluiten',
    'Save' => 'Opslaan',
    'edit' => 'Wijzig',
    'Send' => 'Verstuur',
    'Next' => 'Volgende',
    'Export' => 'Exporteer',
    'Actions' => 'Acties',
];