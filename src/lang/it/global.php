<?php

return [
    'requiredFieldsExplanation' => '',

    'Cancel' => 'Annulla',
    'Save' => 'Salvare',
    'Close' => 'Chiuso',
    'edit' => 'Modificare',
    'Send' => 'Inviare',
    'Next' => 'Il prossimo',
    'Export' => 'Esportare',
    'Actions' => 'Azioni',    
];