<?php

return [
    'requiredFieldsExplanation' => 'Fields with a<span style="color: #ff0000"> * </span>are required',

    'Cancel' => 'Cancel',
    'Save' => 'Save',
];