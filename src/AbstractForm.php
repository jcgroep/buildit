<?php namespace Jcgroep\BuildIt;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\MessageBag;

abstract class AbstractForm
{
    /**
     * @var string 'create' or 'edit' form
     */
    protected $type = 'create';
    /**
     * @var Form The form creator
     */
    protected $form;
    /**
     * @var MessageBag the errors
     */
    protected $errors;
    /**
     * @var Eloquent the target to edit, for filling in the default values
     */
    protected $target;
    protected $data = [];

    /**
     * Make a new form
     * @param MessageBag $errors The errors of the previous filled form
     * @param Eloquent   $target The target to make this edit form for
     * @return static (fluent function)
     */
    public static function make(MessageBag $errors = null, Eloquent $target = null)
    {
        if($errors == null){
            $errors = new MessageBag();
        }
        return new static($errors, $target);
    }

    /**
     * @param MessageBag $errors The errors of the previous filled form
     * @param Eloquent   $target The target to make this edit form for
     */
    public function __construct(MessageBag $errors, Eloquent $target = null)
    {

        $this->errors = $errors;
        $this->target = $target;
    }

    public function withData(array $data)
    {
        $this->data = $data;
        return $this;
    }

    public abstract function renderEditForm();
    public abstract function renderCreateForm();
}
