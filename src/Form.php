<?php namespace Jcgroep\BuildIt;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\MessageBag;
use Jcgroep\BuildIt\FormElements\Controls\CancelButton;
use Jcgroep\BuildIt\FormElements\Controls\ButtonGroup;
use Jcgroep\BuildIt\FormElements\Controls\SubmitButton;
use Jcgroep\BuildIt\FormElements\FormElement;
use Jcgroep\BuildIt\Traits\HtmlTagTrait;

/**
 * Class Form
 * @package App\Html
 */
class Form implements Renderable
{

    use HtmlTagTrait;
    /**
     * @var array of FormElement instances
     */
    protected $elements = [];
    /**
     * @var Modal Draw this form in a modal
     */
    protected $modal;
    /**
     * @var String The target route
     */
    protected $route;
    /**
     * @var MessageBag The errors of the previous filled form
     */
    protected $errors;
    /**
     * @var String the method (POST, GET, PUT)
     */
    protected $method;

    public $id = 'form';
    protected $ajax = false;
    protected $buttons = [];
    protected $contentOnly = false;
    protected $classes = ['form-horizontal'];
    protected $autsave = false;
    /**
     * @var \Eloquent the element to update
     */
    protected $target;

    /**
     * @param string $route The target route
     * @param String $method the method (POST, GET, PUT)
     * @param MessageBag $errors The errors of the previous filled form
     * @return static
     */
    public static function make($route, $method, $errors)
    {
        return new static($route, $method, $errors);
    }

    /**
     * @param string $route The target route
     * @param String $method the method (POST, GET, PUT)
     * @param MessageBag $errors The errors of the previous filled form
     */
    public function __construct($route, $method, $errors)
    {
        $this->route = $route;
        $this->errors = $errors;
        $this->method = $method;
        $this->buttons = new ButtonGroup([new CancelButton(), (new SubmitButton())->withHotkey("ctrl,Enter")]);
        $this->id = substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 10);
    }

    /**
     * Draw the form inside a modal
     * @param Modal $modal The modal to draw it inside
     * @return static (fluent function)
     */
    public function inModal(Modal $modal)
    {
        $this->modal = $modal;

        return $this;
    }

    /**
     * Add an element to the form
     * @param FormElement $element The element to add
     * @return static (fluent function)
     */
    public function addElement(FormElement $element)
    {
        $this->elements[] = $element;
        return $this;
    }

    /**
     * Add multiple elements to the form
     * @param array $formElements
     * @return static
     */
    public function addElements(Array $formElements = [])
    {
        foreach ($formElements as $element) {
            $this->elements[] = $element;
        }
        return $this;
    }

    public function getButtons()
    {
        return $this->buttons;
    }

    public function loadDefaultValuesFromTarget($target)
    {
        $this->target = $target;

        return $this;
    }

    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set the errors of the previous filled form
     * @param MessageBag $errors The errors of the previous filled form
     * @return static (fluent function)
     */
    public function withErrors(MessageBag $errors)
    {
        $this->errors = $errors;
        return $this;
    }

    public function isAjax()
    {
        $this->ajax = true;
        $this->withAttribute('data-remote', true);
        return $this;
    }

    public function hasErrors()
    {
        if (isset($this->errors)) {
            return $this->errors->any();
        }
        return false;
    }

    public function withButtons(ButtonGroup $buttons)
    {
        $this->buttons = $buttons;
        return $this;
    }

    public function getContentOnly()
    {
        $this->contentOnly = true;
        return $this->render();
    }

    /**
     * @return String The html of this form
     */
    public function render()
    {
        $html = '';

        if ($this->contentOnly == false) {
            if (isset($this->modal)) {
                $html .= $this->modal->modalOpen();
                $html .= $this->modal->modalTitle();
            }
        }

        $html .= \Form::open(array_merge([
            'route' => $this->route,
            'method' => $this->method,
            'class' => join(' ', $this->classes),
            'id' => $this->getId(),
            'files' => true,
            'autocomplete' => 'off'
        ], $this->attributes));


        if (isset($this->modal)) {
            $html .= $this->modal->openModalBody();
        }

        foreach ($this->elements as $element) {

            /**
             * @var $element FormElement
             */
            if ($element->hasToBeRendered()) {
                if ($this->autsave) {
                    $element->withAutosave();
                }
                $html .= $element->inForm($this)->render($this->errors);
            }
        }

        if (!isset($this->modal) && !empty($this->buttons)) {
            $html .= $this->buttons;
        }


        if (isset($this->modal)) {
            $html .= $this->modal->closeModalBody();
            $html .= $this->modal->modalFooter();
        }
        $html .= '</form>';
        if ($this->contentOnly == false) {

            if (isset($this->modal)) {
                $html .= $this->modal->modalClose();
            }
        }
        $html .= "<script>if ($('form#$this->id').length > 0) $('form#$this->id')[0].reset();</script>";

        return $html;
    }

    /**
     * @param String $id
     * @return Form
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return String
     */
    public function getId()
    {
        return $this->id;
    }

    public function addButton($button)
    {
        $this->buttons[] = $button;
    }

    public function withAutosave()
    {
        $this->classes[] = 'autosave';
        $this->autsave = true;
        return $this;
    }

    public function __toString()
    {
        try{
            return $this->render();
        }catch (\Throwable $e){
            dd($e->getMessage(), $e->getTraceAsString());
        }
    }

    public function getElements()
    {
        return $this->elements;
    }
}
