<?php namespace Jcgroep\BuildIt;

use Jcgroep\BuildIt\FormElements\Controls\Button;
use Jcgroep\BuildIt\FormElements\Controls\ButtonGroup;
use Lang;

/**
 * Class Modal
 * @package App\Html
 */
class Modal
{
    protected $isOpen = false;
    protected $size = 'modal-large';
    protected $ajaxContentLoad = false;
    /**
     * @var array An array with options, the following options are supported:
     * 'closeButton' => bool, Whether to draw a close button in the top right corner
     */
    private $options;
    /**
     * @var string The title to show
     */
    private $title;
    /**
     * @var string The ID of the modal
     */
    private $id;

    private $form;
    private $attributes = [];
    protected $headerButtons = [];
    protected $footerText = "";

    /**
     * Modal constructor.
     * @param array $buttons an array of the buttons to draw, can be 'cancel', 'save' and 'close'
     * @param String $title The title of the modal
     * @param String $id The ID of the modal
     * @param Form $form
     */
    public function __construct($title, $id, Form $form)
    {
        $this->id = $id;
        $this->options = [
            'closeButton' => true,
        ];
        $this->title = $title;
        $this->form = $form;
        if ($form->hasErrors()) {
            $this->open();
        }
    }

    public function inAjaxModal()
    {
        $this->ajaxContentLoad = true;
        return $this;
    }

    public function open()
    {
        $this->isOpen = true;
        return $this;
    }

    public function withOption($key, $value)
    {
        $this->options[$key] = $value;
        return $this;
    }

    /**
     * Create a new modal
     * @param array $buttons an array of the buttons to draw, can be 'cancel', 'save' and 'close'
     * @param String $title The title of the modal
     * @param String $id The ID of the modal
     * @param Form $form The form it contains
     * @return static
     */
    public static function create($title, $id, Form $form)
    {
        return new self($title, $id, $form);
    }

    public function withDataAttribute($dataAttribute, $value)
    {
        $this->attributes[] = 'data-' . $dataAttribute . '="' . $value . '"';
        return $this;
    }

    public function addAttribute($attribute)
    {
        $this->attributes[] = $attribute;

        return $this;
    }

    public function withSize($size)
    {
        $this->size = $size;
        return $this;
    }

    public function addHeaderButton(ButtonGroup $button)
    {
        $this->headerButtons = $button;
        return $this;
    }

    public function withFooterText($text) {
        $this->footerText = $text;
        return $this;
    }

    /**
     * Get the html for opening the modal
     * @return string the html for opening the modal
     */
    public function modalOpen()
    {
        $html = '';
        if (!$this->ajaxContentLoad) {
            $html .= '<div id="' . $this->id . '" class="modal ' . $this->size . '" data-backdrop="static" role="dialog" ';
            $html .= join(' ', $this->attributes);
            $html .= ' > ';
        }
        $html .= '<div class="modal-content">';

        return $html;
    }

    /**
     * Get the html for the modal title
     * @return string the html for the modal title
     */
    public function modalTitle()
    {
        $html = '<div class="modal-header">';
        if ($this->options['closeButton'] == true) {
            $html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>';
        }
        $html .= '<h4 class="modal-title">' . $this->title;
        if (!empty($this->headerButtons)) {
            $html .= $this->headerButtons;
        }
        $html .= '</h4>';
        $html .= '</div>';
        return $html;
    }

    /**
     * Get the html for the modal footer
     * @return string the html for the modal footer with the buttons
     */
    public function modalFooter()
    {
        $html = '<div class="modal-footer">';
        $html .= '<div class="pull-left" style="font-size: smaller"> ' . trans('BuildIt::global.requiredFieldsExplanation') . ' ' . $this->footerText. '</div>';
        $html .= $this->form->getButtons()->withBaseId($this->id);
        $html .= '<div class="pull-right mt-2"><i class="fa fa-spinner fa-pulse fa-3x fa-fw hidden form-upload-spinner"></i></div>';
        $html .= '</div>';
        return $html;
    }

    /**
     * Get the html for closing the modal
     * @return string The html for closing the modal
     */
    public function modalClose()
    {
        $html = '</div>';
        if (!$this->ajaxContentLoad) {
            $html .= '</div>';
        }
        if ($this->isOpen) {
            $html .= "<script>setTimeout(function(){ $('#$this->id').modal('show'); }, 500)</script>;";
        }
        return $html;
    }


    /**
     * Get the html for opening the modal body
     * @return string The html for opening the modal body
     */
    public function openModalBody()
    {
        return '<div class="modal-body"> <div class="container-fluid">' . view('layouts.partials._flash')->render();
    }

    /**
     * Get the html for closing the modal body
     * @return string The html for closing the modal body
     */
    public function closeModalBody()
    {
        return '</div> </div>';
    }


}
