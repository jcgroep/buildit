<?php namespace Jcgroep\BuildIt\Traits;

use Illuminate\Support\Arr;

trait HtmlTagTrait
{
    protected $attributes = [];

    /**
     * @deprecated
     */
    protected function getDataAttributes()
    {
        $html = ' ';
        foreach ($this->attributes as $attribute => $value) {
            if($value !== null){
                $html .= e($attribute) . '="' . ($value) . '" ';
            }else{
                $html .= e($attribute) . ' ';
            }
        }
        return $html;
    }

    public function hasAttribute($name)
    {
        return array_key_exists($name, $this->attributes);
    }

    public function getAttribute($name)
    {
        return Arr::get($this->attributes, $name);
    }

    protected function getAttributes()
    {
        $html = ' ';
        foreach ($this->attributes as $attribute => $value) {
            $html .= e($attribute) . '="' . e($value) . '" ';
        }
        return $html;
    }

    public function withDataAttribute($key, $value)
    {
        $this->attributes['data-' . $key] = $value;
        return $this;
    }

    public function withAttribute($key, $value = null)
    {
        $this->attributes[$key] = $value;
        return $this;
    }
}