<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Lang;
use App\Utils\Crypt\CryptService;
use Illuminate\Support\Arr;

class ChecklistOtherOption extends OtherOption
{
    public static function make(GroupElement $element, $label)
    {
        return new self($element, $label);
    }

    public function render()
    {
        $html = '<label >';
        $html .= '<input class="radio-button" type="checkbox" name="' . $this->parent->getName() . '[]" id="' . $this->parent->getName() . '-other-checkbox" value="' . ($this->isChecked() ? $this->getOtherValue() : '') . '"' . $this->getCheckedAttribute() . ($this->parent->getRequiredAttribute() . $this->parent->getDisabledAttribute()) . '>';
        $html .= $this->label . ' ' . $this->getInput();
        $html .= '</label>';
        $html .= $this->getJavascript();
        return $html;
    }

    protected function isChecked()
    {
        return $this->getOtherValue() != null;
    }

    protected function getOtherValue() {
        $defaultValue = $this->parent->getDefaultValue();

        if ( CryptService::isEncrypted($defaultValue) ) {
            $defaultValue = CryptService::decryptValue($defaultValue);
        }
        if(!is_array($defaultValue) ){
            $defaultValue = Arr::wrap(json_decode($defaultValue));
        }

        // checkbox list filled in using the App use a different way to save the answer
        if (isset($defaultValue['0']) && is_object($defaultValue['0']))
        {
            $defaultValue = array_keys(get_object_vars($defaultValue['0']));
        }

        $parentOptions = collect($this->parent->getOptions())->pluck('value');

        return collect($defaultValue)->reject( function($option) use ($parentOptions) {
            return $parentOptions->contains($option);
        })->first();
    }

    public function getInput()
    {
        $element = TextElement::create()
            ->withId($this->parent->getName() . '-other-input')
            ->disable()
            ->withInputClass('inline');
        if($this->isChecked()){
            $element->withDefaultValue($this->getOtherValue());
        }

        return $element->renderElement();
    }

    protected function getJavascript()
    {
        if (!$this->parent->isDisabled() ) {
            return '<script>
                $("#' . $this->parent->getName() . '-other-checkbox").change(function(){
                    $("#' . $this->parent->getName() . '-other-input").prop("disabled", !$("#' . $this->parent->getName() . '-other-checkbox").is(":checked"));
                }).trigger("change");
                $("#' . $this->parent->getName() . '-other-input").keyup(function(){
                    $("#' . $this->parent->getName() . '-other-checkbox").val($(this).val()).trigger("change");
                });
                </script>';
        }
    }
}