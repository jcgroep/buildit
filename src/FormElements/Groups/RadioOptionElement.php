<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use Request;
use Jcgroep\BuildIt\Traits\HtmlTagTrait;

class RadioOptionElement
{
    protected static $number = 1;
    use HtmlTagTrait;

    protected $value;
    protected $label;
    protected $subs;
    protected $labelClass;
    protected $defaultValue;
    protected $javascript = '';
    private $parent;

    public static function make($parent, $value, $label)
    {
        return new static($parent, $value, $label);
    }

    public function __construct($parent, $value, $label)
    {
        if (is_array($label)) {
            $this->label = (string) $value;
            $this->subs = $label;

            $this->value = substr($this->label,0, strpos($this->label, " ") );
            $this->withAttribute('value', $this->value );

        } else {
            $this->value = (string) $value;
            $this->label = $label;
            $this->withAttribute('value', $this->value);
        }
        $this->withAttribute('class', 'radio-button');
        $this->withAttribute('type', 'radio');
        $this->parent = $parent;
    }

    public function setRequired()
    {
        return $this->withAttribute('required');
    }

    public function disabled()
    {
        return $this->withAttribute('disabled');

    }

    public function withLabelClass($labelClass)
    {
        $this->labelClass = $labelClass;
        return $this;
    }

    public function withDateInput($default = null)
    {
        $this->withDataAttribute('style', 'margin-top:10px;');
        $this->label = '<span>' . $this->label . ' <input name="date" class="datepicker change-value" value="'. e($default) . '" /></span>';
        $this->withJavascript('$(".change-value").on("change", function(){var input = $(this).parents("label").find(".radio-button"); input.val($(this).val()); input.prop("checked", true)});
                    $(".datepicker").datepicker({format: "dd-mm-yyyy", language: Lang.get("dates.datepickerLocale"), autoclose: true});');
        return $this;
    }

    public function withDefaultValue($value)
    {
        $this->defaultValue = $value;
        return $this;
    }

    public function withName($name)
    {
        $this->withAttribute('name', $name);
        $this->withAttribute('id', $name . self::$number);
        self::$number++;
        return $this;
    }

    public function getCheckedAttribute()
    {
        return $this->isChecked() ? ' checked ' : '';
    }

    public function isChecked()
    {
        if (Request::old($this->getAttribute('name')) !== null) {
            return Request::old($this->getAttribute('name')) == $this->value;
        }
        return $this->value !== null && $this->value == $this->defaultValue;
    }

    public function render()
    {
        $this->generateSubJavascript();
        $html = '<label class="' . $this->labelClass . '">';
        $html .= '<input ' . $this->getAttributes() . $this->getCheckedAttribute() . '>';
        $html .= $this->label;
        if (is_array($this->subs)) {
            foreach ($this->subs as $value => $label) {
                $html .= RadioOptionElement::make($this, $value, $label)
                    ->withDefaultValue($this->defaultValue)
                    ->withName($this->getAttribute('name') . '-sub')
                    ->withLabelClass('sub-radio ' . $this->labelClass)
                    ->render();
            }
        }
        $html .= '</label>';
        $html .= '<script>' . $this->javascript . '</script>';
        return $html;
    }

    public function withJavascript($javascript)
    {
        $this->javascript .= $javascript;
        return $this;
    }

    protected function generateSubJavascript()
    {
        if (!$this->parent instanceof RadioOptionElement) {
            return;
        }
        $this->withJavascript('
            $("#' . $this->getAttribute('id') . '").on("change", function(){
                $("#' . $this->parent->getAttribute('id') . '").prop("checked", true).val($(this).val()).trigger("change");
            });
            setTimeout(function(){$("#' . $this->getAttribute('id') . ':checked").trigger("change");}, 100);
        ');
    }
}