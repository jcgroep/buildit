<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use Jcgroep\BuildIt\FormElements\Text\TextElement;
use Lang;

class OtherOption
{
    protected $parent;
    protected $label;

    public static function make(GroupElement $element, $label)
    {
        return new self($element, $label);
    }

    public function __construct(GroupElement $element, $label)
    {
        $this->parent = $element;
        $this->label = $label;
    }

    public function getInput()
    {
        $element = TextElement::create()
            ->withId($this->parent->getName() . '-other-input')
            ->disable()
            ->withInputClass('inline');
        if($this->isChecked()){
            $element->withDefaultValue($this->parent->getDefaultValue());
        }


        return $element->renderElement();
    }


    public function render()
    {
        $html = '<label class="' . $this->parent->getInputLabelClass() . '">';
        $html .= '<input class="radio-button" type="radio" name="' . $this->parent->getName() . '" id="' . $this->parent->getName() . '-other-radio" value="' . ($this->isChecked() ? $this->parent->getDefaultValue() : '') . '"' . $this->getCheckedAttribute() . ($this->parent->getRequiredAttribute() . $this->parent->getDisabledAttribute()) . '>';
        $html .= $this->label . ' ' . $this->getInput();
        $html .= '</label>';
        $html .= $this->getJavascript();
        return $html;
    }

    protected function isChecked()
    {
        $defaultValue = $this->parent->getDefaultValue();
        if (!empty($defaultValue)) {
            $selectedValues = collect($this->parent->getOptions())->pluck('value')->filter(function ($value) use ($defaultValue) {
                return $value == $defaultValue;
            });
            if ($selectedValues->isEmpty()) {
                return true;
            }
        }
        return false;
    }

    protected function getCheckedAttribute()
    {
        return $this->isChecked() ? ' checked ' : '';
    }

    protected function getJavascript()
    {
        return '<script>
            $("input[name=\'' . $this->parent->getName() . '\'").change(function(){
                $("#' . $this->parent->getName() . '-other-input").prop("disabled", !$("#' . $this->parent->getName() . '-other-radio").is(":checked"));
            }).trigger("change");
            $("#' . $this->parent->getName() . '-other-input").keyup(function(){
                $("#' . $this->parent->getName() . '-other-radio").val($(this).val()).trigger("change");
            });
            </script>';
    }
}