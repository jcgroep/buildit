<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use App\Utils\Crypt\CryptService;
use Illuminate\Support\Collection;
use Jcgroep\BuildIt\FormElements\FormElement;
use Lang;
use Request;
use Illuminate\Support\Arr;

abstract class GroupElement extends FormElement
{

    /**
     * @var array of arrays, the inner array contains 2 keys, the label and the value. This are all options to be rendered
     */
    protected $inputs = [];

    /**
     * @var Boolean if element has simple options, set by function withSimpleOptions
     */
    protected $hasSimpleOptions = false;

    public function getOptions()
    {
        return $this->inputs;
    }

    /**
     * Add an input option
     * @param string $optionLabel The label to show
     * @param string $value The value when selected
     * @return self (fluid function)
     */
    public function addOption($optionLabel, $value = '', $groupName = null)
    {
        if ($groupName == null) {
            $this->inputs[] = [
                'label' => $optionLabel,
                'value' => $value,
            ];
        } else {
            if (array_key_exists($groupName, $this->inputs)) {
                $this->inputs[$groupName][] = [
                    'label' => $optionLabel,
                    'value' => $value,
                ];
            } else {
                $this->inputs[$groupName] = [[
                    'label' => $optionLabel,
                    'value' => $value,
                ]];
            }

        }

        return $this;
    }

    /**
     * Add the options by an collection of eloquent objects
     * @param Collection $collection The collection to add
     * @param string $nameAttribute The attribute of the model to use for the translation (default 'name')
     * @return static (fluid function)
     */
    public function withCollection(Collection $collection, $nameAttribute = 'name')
    {
        foreach ($collection as $index => $instanceOrCollection) {
            if($instanceOrCollection instanceof Collection){
                foreach($instanceOrCollection as $instance){
                    $this->addOption($instance[$nameAttribute], $instance->id, $index);
                }
            }else{
                $this->addOption($instanceOrCollection[$nameAttribute], $instanceOrCollection->id);
            }
        }
        return $this;
    }

    /**
     * Add the options by an collection of eloquent objects
     * @param Array $simpleOptions The names of the options to add
     * @param string $translation for if the options have a translation for the label
     * @param string $groupName
     * @return static
     */
    public function withSimpleOptions(Array $simpleOptions, $translation = null, $groupName = null)
    {
        $this->hasSimpleOptions = true;
        foreach ($simpleOptions as $key => $optionName) {
            if (is_array($optionName)) {
                $this->withSimpleOptions($optionName, $translation, $key);
            } else {
                $this->addOption(isset($translation) ? trans($translation . '.' . $optionName) : $optionName, $optionName, $groupName);
            }
        }
        return $this;
    }

    /**
     * @param array $options as key the value in the database and as value the label
     * @return static
     */
    public function withArray(array $options)
    {
        foreach($options as $key => $value){
            $this->addOption($value, $key);
        }
        return $this;
    }

    /**
     * Check the old input and the default input to get the checked attribute.
     * @param array $checkbox an array with 2 keys, 'value' and 'label'.
     * @return string 'checked' if the attribute is checked, an empty string if not
     */
    protected function getCheckedAttribute($checkbox)
    {
        if (!empty(Request::old($this->name))) {
            if (is_array(Request::old($this->name))) {
                return in_array($checkbox['value'], Request::old($this->name)) ? ' checked' : '';
            }
            return $checkbox['value'] == Request::old($this->name) ? ' checked' : '';
        }

        $defaultValue = $this->getDefaultValue();

        if ( CryptService::isEncrypted($defaultValue) ) {
            $defaultValue = CryptService::decryptValue($defaultValue);
        }
//        dd($defaultValue);
        if (empty($defaultValue)) {
            return '';
        }
        $valueObjectField = is_int($checkbox['value']) ? 'id' : 'name';
        if($defaultValue instanceof Collection){
            return in_array($checkbox['value'], $defaultValue->pluck($valueObjectField)->all()) ? ' checked' : '';
        }

        if(!is_array($defaultValue) ){
            $defaultValue = Arr::wrap(json_decode($defaultValue));
        }

        // checkbox list filled in using the App use a different way to save the answer
        if (isset($defaultValue['0']) && is_object($defaultValue['0']))
        {
            $defaultValue = array_keys(get_object_vars($defaultValue['0']));
        }

        return in_array($checkbox['value'], $defaultValue) ? ' checked' : '';
    }
}
