<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use Lang;

class BooleanElement extends RadioElement
{
    public function __construct()
    {
        parent::__construct();
        $this->addOption(trans('global.Yes'), 1);
        $this->addOption(trans('global.No'), 0);
    }
}