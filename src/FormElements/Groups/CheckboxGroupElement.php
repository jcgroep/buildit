<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use Illuminate\Support\MessageBag;
use Lang;
use Mews\Purifier\Facades\Purifier;

/**
 * Class CheckboxGroupElement
 * @package App\Html\FormElements
 */
class CheckboxGroupElement extends GroupElement
{
    protected $withOther = '';

    public function withOther($label = null)
    {
        if($label == null) {
            $label = trans('BuildIt::global.otherNamed');
        }
        $this->withOther = $label;
        return $this;
    }

    protected function getCheckedAttribute($checkbox)
    {
        $checked = parent::getCheckedAttribute($checkbox);

        if(count($this->inputs) == 1 && $this->required){
            return ' checked';
        }
        return $checked;
    }

    public function render(MessageBag $errors=null)
    {
        $this->classes[] = 'checkbox-group';
        return parent::render($errors);
    }
    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $nameField = $this->hasSimpleOptions ? 'value' : 'label';
        $html = '';
        foreach ($this->inputs as $checkbox) {
            $html .= '<div class="checkbox">';
            $html .= '<label>';
            $html .= '<input type="checkbox" name="' . $this->name . '[]" id="checkbox-' . $checkbox['value'] . '" value="' . e($checkbox['value']) . '" ' . $this->getCheckedAttribute($checkbox) . $this->disabled . '>';
            $html .= substr(Purifier::clean($checkbox['label']), 3, -4);
            $html .= '</label>';
            $html .= '</div>';
        }

        if(!empty($this->withOther)){
            $html .= ChecklistOtherOption::make($this, $this->withOther)->render();

        }
        return $html;
    }

    protected function getVisibilityJs($value, $operator)
    {
       return '$.inArray("' .trim($value, '\'"'). '", (answers["' . $this->name . '"] || "").split(",")) > -1';
    }

    protected function getJsValueFunction($setValue = null)
       {
           if ($setValue === null) {
               return 'find(":input").removeAttr("checked")';
           }
           return 'find(":input").each(function(){
                $(this).prop("checked", in_array(this.value, "' . $setValue . '".split(";")))
           })';
       }
}
