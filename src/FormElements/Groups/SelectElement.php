<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use Illuminate\Support\Collection;
use Request;

class SelectElement extends GroupElement
{

    /**
     * @var bool Allow for selecting multiple values or not
     */
    protected $multiple = false;
    protected $search = true;
    protected $selectId = null;
    protected $allowNewInputs = false;
    protected $showEmpty = true;

    public function __construct() {
        $this->withAttribute('class', 'form-control');
    }
    /**
     * Allow for select multiple options
     * @return static (fluid function)
     */
    public function allowMultiple()
    {
        $this->multiple = true;
        return $this;
    }

    public function hideEmpty()
    {
        $this->showEmpty = false;
        return $this;
    }

    public function withoutSearch()
    {
        $this->search = false;
        return $this;
    }

    public function withId($id)
    {
        $this->selectId = $id;
        return $this;
    }

    public function allowNewInputs()
    {
        $this->allowNewInputs = true;
        return $this;
    }

    public function withOther() {
        return $this->allowNewInputs();
    }

    /**
     * Check the old input and the default input to get the selected attribute.
     * @param array $option an array with 2 keys, 'value' and 'label'.
     * @return string 'selected' if the attribute is checked, an empty string if not
     */
    protected function getSelectedAttribute($option)
    {
        if (!empty(Request::old($this->name))) {
            if (is_array(Request::old($this->name))) {
                return in_array($option['value'], Request::old($this->name)) ? ' selected="selected"' : '';
            }
            return $option['value'] == Request::old($this->name) ? ' selected="selected"' : '';
        } elseif (!empty($this->getDefaultValue())) {
            if ($this->getDefaultValue() instanceof Collection) {
                return in_array($option['value'], $this->getDefaultValue()->pluck('id')->all()) ? ' selected="selected"' : '';
            }
            return $option['value'] == $this->getDefaultValue() ? ' selected="selected"' : '';

        }
        return '';
    }

    public function getSelectId()
    {
        return $this->selectId ?? $this->name;
    }

    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $this->withAttribute('name', $this->name . ($this->multiple ? '[]' : ''));
        $this->withAttribute('placeholder', strip_tags($this->label));
        $this->withAttribute('id', $this->getSelectId());
        if($this->multiple){
            $this->withAttribute('multiple');
        }

        if($this->required){
            $this->withAttribute('required');
        }

        if ($this->allowNewInputs) {
            //dd($this->getDefaultValue(), array_keys($this->inputs), in_array($this->getDefaultValue(), array_keys($this->inputs), true ));
            if ( !in_array($this->getDefaultValue(), array_keys($this->inputs), true )) {
                $this->inputs[] = ['value' => $this->getDefaultValue(), 'label' => $this->getDefaultValue()];
            }
        }

        $html = '<select ' . $this->disabled . $this->getAttributes() . '>';
        if (!$this->multiple && $this->showEmpty) {
            $html .= '<option disabled ' . ($this->getDefaultValue() == null ? 'selected' : '') .' value hidden></option>';
        }
//        $html .= '<option disabled selected hidden value="' . $this->getDefaultValue() . '">' . $this->getDefaultValue() . '</option>';
        foreach ($this->inputs as $key => $option) {
            $html .= $this->renderOption($key, $option);
        }
        $html .= '</select>';

        if(isset($this->postfix)){
            $html .= "&nbsp; " . $this->postfix;
        }

        return $html;
    }

    protected function renderOption($key, $option)
    {
        $html = '';
        if (is_array($option) && !array_key_exists('label', $option)) {
            $html .= '<optgroup label="' . e($key) . '">';
            foreach ($option as $key => $subOption) {
                $html .= $this->renderOption($key, $subOption);
            }
            $html .= '</optgroup>';
        } else {
            $html .= '<option value="' . e($option['value']) . '" ' . $this->getSelectedAttribute($option) . '>' . e($option['label']) . '</option>';
        }
        return $html;
    }

    public function getJsSelector()
    {
        return '#' . $this->getId() . ' select';
    }

    public function getJavascript()
    {
        $js = parent::getJavascript();
        if ($this->allowNewInputs) {
            $js .= '$("#' . $this->getId() . ' select").select2({
                width: "100%",
                tags: true,
                selectOnClose: true,
                createTag: function (params) {
                    return {
                        id: params.term,
                        text: params.term,
                        newOption: true
                    }
                },
                templateResult: function (data) {
                    var $result = $("<span></span>");

                    $result.text(data.text);

                    if (data.newOption) {
                        $result.append(" <em>(nieuw)</em>");
                    }

                    return $result;
                }
            });';

        } elseif ($this->search) {
            $js .= '$("#' . $this->getId() . ' select").select2({
                    width: "100%",
                });';
        } else {
            $js .= '$("#' . $this->getId() . ' select").select2({
                    width: "100%",
                    minimumResultsForSearch: Infinity
                });';
        }

        return $js;
    }
}
