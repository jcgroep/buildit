<?php namespace Jcgroep\BuildIt\FormElements\Groups;

class ButtonsElement extends GroupElement
{
    protected $directSubmit = false;

    public function submitDirectly()
    {
        $this->directSubmit = true;
        return $this;
    }

    public function suppressSubmission()
    {
        $this->directSubmit = false;
        return $this;
    }

    protected function getActiveAttribute($radio)
    {
        return $radio['value'] == $this->getDefaultValue() ? 'blue' : 'gray';
    }

    public function renderElement()
    {
        $attributes = "";

        if ($this->hasAttribute('wire:model.lazy'))
        {
            $attributes .= ' wire:model.lazy="' . $this->getAttribute('wire:model.lazy') . '"';
        }

        if ($this->hasAttribute('wire:model'))
        {
            $attributes .= ' wire:model="' . $this->getAttribute('wire:model') . '"';
        }

        $html = '<div class="button-radio">';
        $html .= '<input class="hidden" id="' . $this->name . '" name="' . $this->name . '" value="' . e($this->getDefaultValue()) . '" '. $attributes . '/>';
        foreach ($this->inputs as $option) {
            $html .= '<button class="btn autocommit ' . $this->getActiveAttribute($option) . ' '. $this->disabled .'" data-value="' . e($option['value']) . '" data-target="' . $this->name . '"';

            if ($this->hasAttribute('wire:model') || 1)
            {
                $html .= ' wire:click="'. $this->name. '(\''. e($option['value']). '\');"';
            }

            $html .= '>';
            $html .= $option['label'];
            $html .= '</button>';
        }
        $html .= '</div>';
        return $html;
    }

    public function getJavascript()
    {
        $html = parent::getJavascript();
        $html .= '$("#' . $this->name . ' button.autocommit").on("click", function(e){';
        $html .= '  console.log("Wawawa");';
        $html .= '  e.preventDefault();';
        $html .= '  $(":input[name=" + $(this).data("target") + "]").val($(this).data("value")).trigger("change");;';
        $html .= '  $(this).parents(".button-radio").find("button.autocommit").removeClass("blue").addClass("gray");';
        $html .= '  $(this).removeClass("gray").addClass("blue");';
        if ($this->directSubmit) {
            $html .= '  $(this).parents("form").submit();';
        }
        $html .= '});';
        return $html;
    }


}