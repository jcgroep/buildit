<?php namespace Jcgroep\BuildIt\FormElements\Groups;

use Request;
use Lang;

/**
 * Class RadioElement
 * @package App\Html\FormElements
 */
class RadioElement extends GroupElement
{
    protected $withOther = '';
    private $withChangeJavascript = false;
    private $inputLabelClass;

    public function getInputLabelClass()
    {
        return $this->inputLabelClass;
    }

    public function __construct()
    {
        $this->inputLabelClass = 'radio-inline';
    }

    public function oneOptionPerLine()
    {
        $this->inputLabelClass = 'radio';
        $this->withClasses(['col-md-8']);

        return $this;
    }

    public function withOther($label = null)
    {
        if($label == null) {
            $label = trans('BuildIt::global.otherNamed');
        }
        $this->withOther = $label;
        return $this;
    }
    /**
     * Check the old input and the default input to get the checked attribute.
     * @param array $radio an array with 2 keys, 'value' and 'label'.
     * @return string 'checked' if the attribute is checked, an empty string if not
     */
    protected function getCheckedAttribute($radio)
    {
        if (Request::old($this->name) !== null) {
            return Request::old($this->name) == $radio['value'] ? ' checked' : '';
        }
        return $radio['value'] == $this->getDefaultValue() ? ' checked' : '';
    }

    public function forNowOrLater($nowLabel, $onLabel, $dateDefault)
    {
        $this->inputs[] = [
            'label' => $nowLabel,
            'value' => 'now'
        ];
        $this->inputs[] = [
            'label' => $onLabel,
            'value' => 'at',
            'withDateInput' => true,
            'defaultValue' => $dateDefault
        ];
        $this->inputLabelClass = 'radio';
        $this->withLabelClass('control-label now-or-later col-md-3');
        $this->withClasses(['col-md-8']);
        return $this;
    }

    public function getDefaultValue() {
        if ( parent::getDefaultValue() === null) {
            return "";
        }

        return parent::getDefaultValue();
    }

    protected function renderOption($radio)
    {
        $option = RadioOptionElement::make($this, $radio['value'], $radio['label'])
            ->withDefaultValue($this->getDefaultValue())
            ->withName($this->name)
            ->withLabelClass($this->inputLabelClass);

        if($this->isDisabled()){
            $option->disabled();
        }

        if ($this->hasAttribute('wire:model.lazy'))
        {
            $option->withAttribute('wire:model.lazy', $this->getAttribute('wire:model.lazy'));
        }

        if ($this->hasAttribute('wire:model'))
        {
            $option->withAttribute('wire:model', $this->getAttribute('wire:model'));
        }

        if(array_key_exists('withDateInput', $radio)){
            $option->withDateInput(isset($radio['defaultValue']) ? $radio['defaultValue']->format("d-m-Y") : null);
        }
        return $option->render();
    }
    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $html = '<div class="radio-list">';

        foreach ($this->inputs as $name => $radio) {
            if (isset($radio['value'])) {
                $html .= $this->renderOption($radio);
            }  else {
                $name = $name;
                $html .= "<b>" . $name . "</b>";

                foreach( $radio as $subRadio)
                {
                    $html .= $this->renderOption($subRadio);
                }
            }
        }
        if(!empty($this->withOther)){
            $html .= OtherOption::make($this, $this->withOther)->render();

        }
        $html .= '</div>';
        if ($this->withChangeJavascript) {
            $html .= '<script>$(".change-value").on("change", function(){var input = $(this).parents("label").find(".radio-button"); input.val($(this).val()); input.prop("checked", true)});</script>';
            $html .= '<script>$(".datepicker").datepicker({format: "dd-mm-yyyy", language: Lang.get("dates.datepickerLocale"), autoclose: true});</script>';
        }
        return $html;
    }



    protected function getJsValueFunction($setValue = null)
    {
        if ($setValue === null) {
            return 'is(":checked").val()';
        }
        return 'prop("checked",' . (filter_var($setValue, FILTER_VALIDATE_BOOLEAN) ? 'true' : 'false') . ')';
    }
    
    public function hasWithOther() {
        return $this->withOther != null;
    }
}
