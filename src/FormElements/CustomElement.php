<?php namespace Jcgroep\BuildIt\FormElements;

/**
 * Class CustomElement for creating a custom element
 * @package App\Html\FormElements
 */
class CustomElement extends FormElement
{
    /**
     * @var string The html of this form element
     */
    protected $html;

    /**
     * @param string $html
     * @return static (fluent function)
     */
    public function withHtml($html)
    {
        $this->html = $html;
        return $this;
    }

    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        return $this->html;
    }

}
