<?php namespace Jcgroep\BuildIt\FormElements\Text;

class TextElement extends InputElement
{
    /**
     * TextElement constructor, set the type of the input as text.
     */
    public function __construct()
    {
        $this->forType('text');
    }
}
