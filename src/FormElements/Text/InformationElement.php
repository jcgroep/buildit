<?php namespace Jcgroep\BuildIt\FormElements\Text;

use Jcgroep\BuildIt\FormElements\FormElement;

/**
 * Class informationElement
 * @package App\Html\FormElements\Text
 */
class InformationElement extends FormElement
{

    /**
     * @var string The text of the input field
     */
    protected $text;


    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return informationElement
     */
    public function withText($text)
    {
        $this->text = $text;
        return $this;
    }
}
