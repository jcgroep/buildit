<?php namespace Jcgroep\BuildIt\FormElements\Text;

use Illuminate\Support\Str;

class TableElement extends InputElement
{
    protected $rows;
    protected $header;

    public function withRows($rows) {
        $this->rows = $rows;
        
        return $this;
    }

    public function withHeaders($headers) {
        $this->headers = $headers;

        return $this;
    }

    public function renderElement()
    {
        ob_start();

        $prefix = Str::random(8);

        echo HiddenInput::create()
            ->withRowId($this->name)
            ->withName($this->name)
            ->withId($prefix . "_" . $this->name)
            ->withDefaultValue(htmlentities($this->defaultValue))
            ->renderElement();

        echo "<table class=\"table\" border=1 id=\"". $prefix . "_table_". $this->name . "\"><thead><tr>";
        foreach( $this->getHeaders() as $header) {
            echo "<th>". $header . "</th>";
        }
        echo "</tr></thead><tbody>";
        for( $i= 1; $i <= $this->rows; $i++) {
            echo "<tr>";
            for( $j = 0; $j < $this->getHeaders()->count(); $j++ ) {
                echo "<td>";

                if ( $this->getCellTextValue($i,$j) ) {
                    echo $this->getCellTextValue($i,$j);
                } else {
                    $element = GrowingTextAreaElement::create()
                    ->withClasses(['questionnaire-table'])
                    ->withAttribute('data-identifier', $this->name . "_" . $i . "_" . $j)
                    ->withRowId($prefix . "_" . $this->name . "_" . $i . "_" . $j)
                    ->withCols("")
                    
                    ->withDefaultValue($this->getCellValue($i, $j) );
                    
                if ( $this->isDisabled() ) {
                    $element->disable();
                }    
                echo $element->renderElement();  
                }

                echo "</td>";
            }
            echo "</tr>";
        }
        
        echo "</tbody></table>";

        ?>

<script>

$('#<?php echo $prefix ?>_table_<?php echo $this->name; ?> textarea').on('change keyup paste', function() {
    var data = {};
    $("#<?php echo $prefix ?>_table_<?php echo $this->name; ?> textarea").each(function(){
        data[$(this).data('identifier')] = this.value;
    });
    $('#<?php echo $prefix ?>_<?php echo $this->name; ?>').val( JSON.stringify(data));

    var form = $('#<?php echo $prefix ?>_<?php echo $this->name; ?>').closest('form')
    if (form.hasClass('autosave') ) {
        $('#<?php echo $prefix ?>_<?php echo $this->name; ?>').trigger("change");
    }

});

function delay(callback, ms) {
    var timer = 0;
    return function() {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

// Example usage:

$('#table_<?php echo $this->name; ?> textarea').keyup(delay(function (e) {
    saveTable<?php echo $this->name; ?>();
}, 500));

function saveTable<?php echo $this->name; ?>()
{
    var data = {};
    $("#table_<?php echo $this->name; ?> textarea").each(function(){
        data[this.id] = this.value;
    });
    $('input[name=\'<?php echo $this->name; ?>\']').val( JSON.stringify(data));

    var form = $('input[name=\'<?php echo $this->name; ?>\']').closest('form')
    if (form.hasClass('autosave') ) {
        $('input[name=\'<?php echo $this->name; ?>\']').trigger("change");
    }
}

</script>
        <?php
        return ob_get_clean();
        return $this->headers;
    }

    protected function getHeaders() {
        return $this->parseHeaders()->first();
    }

    protected function parseHeaders() {
        $rows = collect(explode("\r\n", $this->headers)); 
        return $rows->map( function ($row) {
            return collect(explode(',', $row));
        });
    }

    public function getCellTextValue($x,$y) {
        if ( isset($this->parseHeaders()[$x]) && isset($this->parseHeaders()[$x][$y]) ) {
            return $this->parseHeaders()[$x][$y];
        }

    }

    protected function getCellValue($x, $y) {
        $data = json_decode($this->defaultValue);

        $string = $this->name . "_" . $x . "_" . $y;
        if ( isset($data->$string) ) {
            return $data->$string;
        }
    }
}
