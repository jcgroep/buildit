<?php namespace Jcgroep\BuildIt\FormElements\Text;

class TimeElement extends InputElement
{
    public function __construct()
    {
        $this->withClasses(['time-element']);
        $this->withInputClass('time');
        $this->forType('time');
    }

    protected function getPlaceHolder()
    {
        return 'hh:mm';
    }

    public function getJavascript()
    {
        $js = parent::getJavascript();

        $js .= '';

        return $js;
    }
}