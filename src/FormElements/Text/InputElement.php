<?php namespace Jcgroep\BuildIt\FormElements\Text;

use Jcgroep\BuildIt\FormElements\FormElement;

/**
 * Class InputElement
 * @package App\Html\FormElements\Text
 */
class InputElement extends FormElement
{

    /**
     * @var string The type of the input, can be any html input type
     */
    protected $type = 'text';
    /**
     * @var array The classes of the input
     */
    protected $inputClass = ['form-control'];

    protected $element;
    protected $optionalAttributes = [];
    protected $inputId;
    protected $placeholder;

    public function withPlaceholder($placeholder) {
        $this->placeholder = $placeholder;
        return $this;
    }

    /**
     * Set the type attribute of the input
     * @param string $type The type of the input, can be any html input type
     * @return static (fluent function)
     */
    public function forType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function withElement(FormElement $element)
    {
        $this->element = $element;
        return $this;
    }

    public function withId($id)
    {
        $this->inputId = $id;
        return $this;
    }

    public function withInputClass($inputClass)
    {
        $this->inputClass[] = $inputClass;
        return $this;
    }

    protected function getPlaceHolder()
    {
        return $this->placeholder != null ? $this->placeholder : $this->label;
    }

    public function withAutofocus()
    {
        return $this->withAttribute('autofocus');
    }

    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $html = '<input ';
        $html .= 'class="' . join(' ', $this->inputClass) . '" ';
        if(isset($this->postfix)){
            $html .= 'style="display: inline-block;" ';
        }
        $html .= 'placeholder="' . $this->getPlaceHolder() . '" ';
        $html .= 'name="' . $this->name . '" ';
        $html .= 'id="' . (isset($this->inputId) ? $this->inputId : $this->name) . '" ';
        $html .= 'type="' . $this->type . '" ';
        $html .= 'value="' . e($this->getDefaultValue()) . '" ';
        $html .= $this->getAttributes();

        foreach($this->optionalAttributes as $attribute => $value){
            $html .= e($attribute) . '="' . e($value) . '" ';
        }
        $html .= ($this->required ? ' required' : '');
        $html .= $this->disabled;
        $html .= '>';

        if(isset($this->postfix)){
            $html .= "&nbsp; <span id='postfix-". (isset($this->inputId) ? $this->inputId : $this->name) . "'>" . $this->postfix . "</span>";
        }
        return $html;
    }


}
