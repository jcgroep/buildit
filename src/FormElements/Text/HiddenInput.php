<?php namespace Jcgroep\BuildIt\FormElements\Text;
use Form;
use Illuminate\Support\MessageBag;

/**
 * Class HiddenInput
 * @package App\Html\FormElements\Text
 */
class HiddenInput extends InputElement{
    /**
     * The constructor of the HiddenInput adds the type 'hidden' to the input element.
     */
    public function __construct()
    {
        $this->forType('hidden');
    }

    /**
     * Render this element
     * @param MessageBag $errors
     * @return string the html of this input element
     */
    public function render(MessageBag $errors = null)
    {
        return Form::hidden($this->name, $this->defaultValue, ['id' => $this->inputId] );
    }
}
