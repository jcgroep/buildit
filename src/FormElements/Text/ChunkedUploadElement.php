<?php namespace Jcgroep\BuildIt\FormElements\Text;

class ChunkedUploadElement extends UploadElement
{
    protected $multiple = false;
    public function __construct()
    {
        parent::__construct();
        $this->withId('upload-element');
    }

    public function allowMultipleUploads() {
        $this->multiple = true;

        return $this;
    }

    public function renderElement()
    {
        if ($this->multiple) {
            $this->withAttribute("data-multiple", 1);
            $this->withAttribute("multiple", 1);
        } else {
            $this->withAttribute("data-multiple", 0);
        }

        $return = "<div id='uploads'></div>" . parent::renderElement();
        ob_start();
?>
        <input type="hidden" id="upload_identifiers" name="upload_identifiers" autocomplete="false">
        <div id="progress" class="border-gray-300 bg-gray-100 p-3 mt-2">
            <div id="progress-name"  style="width: 300px;" class="inline-block mr-4"></div>
            <div id="progress-status" class="inline-block ml-4"></div>
        </div>

        <script src="/chunked-upload.js"></script>
        <?php
        $return .= ob_get_clean();

        return $return;
    }
}
