<?php namespace Jcgroep\BuildIt\FormElements\Text;

/**
 * Class EmailElement
 * @package App\Html\FormElements\Text
 */
class EmailElement extends InputElement
{
    /**
     * The constructor of the EmailElement adds the type 'email' to the input element.
     */
    public function __construct()
    {
        $this->forType('email');
    }
}
