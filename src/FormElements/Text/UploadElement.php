<?php namespace Jcgroep\BuildIt\FormElements\Text;

use App\Models\Utils\ValueObjects\Files\MimeTypeSupport;

/**
 * Class EmailElement
 * @package App\Html\FormElements\Text
 */
class UploadElement extends InputElement
{
    /**
     * The constructor of the EmailElement adds the type 'email' to the input element.
     */
    public function __construct()
    {
        $this->forType('file');
        $this->optionalAttributes['accept'] = '*';
    }

    public function allow($type)
    {
        if ($this->optionalAttributes['accept'] == '*') {
            $this->optionalAttributes['accept'] = $type;
        } else {
            $this->optionalAttributes['accept'] = $this->optionalAttributes['accept'] . ',' . $type;
        }
        return $this;
    }

    public function allowVideo()
    {
        return $this->allow('video/*');

    }

    public function allowImage()
    {
        return $this->allow('image/*');
    }

    public function allowAudio()
    {
        return $this->allow('audio/*');
    }

    public function allowPdf()
    {
        foreach (MimeTypeSupport::pdf() as $mimeType) {
            $this->allow($mimeType);
        }
        return $this;
    }

    public function allowOfficeFiles()
    {
        foreach (MimeTypeSupport::officeFiles() as $mimeType) {
            $this->allow($mimeType);
        }
        return $this;
    }
}
