<?php namespace Jcgroep\BuildIt\FormElements\Text;

/**
 * Class TextAreaElement
 * @package App\Html\FormElements\Text
 */
class GrowingTextAreaElement extends TextAreaElement
{
    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $this->withAttribute("onInput", "this.parentNode.dataset.replicatedValue = this.value");
//        $this->withAttribute("style", "width: 100%");
        return '<div class="grow-wrap" data-replicated-value="'. $this->getDefaultValue(). '"><textarea ' .
//            'class="' . join(' ', $this->inputClass) . '" ' .
            'placeholder="' . $this->getPlaceHolder() . '" ' .
            'name="' . $this->name . '" ' .
            'id="' . $this->id . '" ' .
            $this->getAttributes() .
            $this->disabled .
            '>' .
            $this->getDefaultValue() .
            '</textarea></div>' . $this->getStyle();
    }

    protected function getStyle()
    {
        ob_start();
        ?>
        <style>
            .grow-wrap {
                /* easy way to plop the elements on top of each other and have them both sized based on the tallest one's height */
                display: grid;
            }
            .grow-wrap::after {
                /* Note the weird space! Needed to preventy jumpy behavior */
                content: attr(data-replicated-value) " ";
                /* This is how textarea text behaves */
                white-space: pre-wrap;

                /* Hidden from view, clicks, and screen readers */
                visibility: hidden;
            }
            .grow-wrap > textarea {
                /* You could leave this, but after a user resizes, then it ruins the auto sizing */
                resize: none;

                /* Firefox shows scrollbar on growth, you can hide like this. */
                overflow: hidden;
            }
            .grow-wrap > textarea,
            .grow-wrap::after {
                /* Identical styling required!! */
                border: 1px solid #777777;
                padding: 0.5rem;
                font: inherit;

                /* Place on top of each other */
                grid-area: 1 / 1 / 2 / 2;
            }
        </style>
<?php
        return ob_get_clean();
    }
}
