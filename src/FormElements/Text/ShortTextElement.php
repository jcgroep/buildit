<?php namespace Jcgroep\BuildIt\FormElements\Text;

class ShortTextElement extends InputElement
{
    public function __construct()
    {
        $this->forType('text');
        $this->withClasses(['col-md-2', 'col-xs-2', 'col-sm-2', 'text-input']);
    }
}
