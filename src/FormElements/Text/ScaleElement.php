<?php namespace Jcgroep\BuildIt\FormElements\Text;

use Illuminate\Support\MessageBag;

class ScaleElement extends InputElement
{
    protected $options;
    protected $scalePostfix;
    protected $scalePrefix;
    protected $showValues = true;

    public function __construct()
    {
        parent::withId(substr(str_shuffle("abcdefghijklmnopqrstuvwxyz"), 0, 10));
    }

    public function withOptions(array $options)
    {
        $this->options = $options;
        return $this;
    }

    public function withScalePrefix($prefix)
    {
        $this->scalePrefix = $prefix;
        return $this;
    }

    public function withScalePostfix($postfix)
    {
        $this->scalePostfix = $postfix;
        return $this;
    }

    public function withId($id)
    {
        throw new \InvalidArgumentException('Own ID managing system, don\'t change!');
    }

    public function isDisabled()
    {
        if($this->disabled){
            return 'true';
        }
        return 'false';
    }

    public function render(MessageBag $errors = null)
    {
        $this->javascript = $this->getScaleJavascript();
        return parent::render($errors);
    }

    public function getDefaultValue()
    {
        return array_search(parent::getDefaultValue(), array_keys($this->options));
    }

    public function hideValues()
    {
        $this->showValues = false;
        return $this;
    }

    public function renderElement() {
        $html = parent::renderElement();
        $html .= '<div style="margin-top: 20px;"><span>'. $this->scalePrefix .'</span>';
        $html .= '<span style="float: right;">'. $this->scalePostfix .'</span></div>';
        return $html;
    }
    private function getScaleJavascript()
    {
        return '
            $("#' . $this->getId() . ' input").ionRangeSlider({
                type: "single",
                grid: ' . ($this->showValues ? 'true' : 'false') . ',
                hide_from_to: true,
                hide_min_max: true,
                force_edges: true,
                from: "' . $this->getDefaultValue() . '",
                disable: ' . $this->isDisabled() . ',
                values: ' . json_encode(array_keys($this->options)) . '
            });';
    }
}