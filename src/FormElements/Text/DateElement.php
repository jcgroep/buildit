<?php namespace Jcgroep\BuildIt\FormElements\Text;

use Carbon\Carbon;

/**
 * Class DateElement
 * @package App\Html\FormElements\Text
 */
class DateElement extends InputElement
{
    /**
     * @var Carbon The default date
     */
    protected $defaultValue;

    /**
     * The constructor of the DateElement adds a datepicker class to the input, makes the column smaller and adds an data attribute with data-date-autoclose to the input.
     */
    public function __construct()
    {
//        		$this->forType('date'); //gives a double date picker in newer browsers...
        $this->inputClass[] = 'datepicker';
        $this->withClasses(['date-input',"col-md-4","col-xs-4","col-sm-4"]);
        $this->withDataAttribute('date-autoclose', '1');
    }

    protected function getPlaceHolder()
    {
        return 'dd-mm-yyyy';
    }

    public function getDefaultValue()
    {
        $default = parent::getDefaultValue();
        if ($default instanceof Carbon) {
            return Carbon::parse(parent::getDefaultValue())->format('d-m-Y');
        }
        if (strlen($default) == 10) {
            return $default;
        }

        return null;
    }

    public function onlyPastDates()
    {
        $this->withDataAttribute('max-date', Carbon::now()->format('d-m-Y'));
    }

    public function onlyFutureDates()
    {
        $this->withDataAttribute('min-date', Carbon::now()->format('d-m-Y'));
    }
}
