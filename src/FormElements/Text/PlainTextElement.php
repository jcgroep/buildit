<?php namespace Jcgroep\BuildIt\FormElements\Text;

class PlainTextElement extends InputElement
{
    public function renderElement()
    {
        return "<span id='" . $this->name . "'>" . $this->getDefaultValue() . "</span>";
    }
}
