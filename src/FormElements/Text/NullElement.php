<?php namespace Jcgroep\BuildIt\FormElements\Text;

class NullElement extends HiddenInput
{
    public function __construct()
    {
        parent::__construct();
        $this->onlyWhen(false);
    }
}