<?php namespace Jcgroep\BuildIt\FormElements\Text;

/**
 * Class TextAreaElement
 * @package App\Html\FormElements\Text
 */
class TextAreaElement extends InputElement
{

    protected $summernote = false;
    /**
     * @var int The number of columns of the text area
     */
    protected $cols = 60;
    /**
     * @var int The number of rows of the text area
     */
    protected $rows = 3;

    /**
     * Turn on summernote for this text area element
     */
    public function withSummerNote()
    {
        $this->withClasses(['col-md-12']);
        $this->withInputClass('summernote');
        $this->summernote = true;
		return $this;
    }

    /**
     * Set the number of columns for the text area
     * @param int $cols The number of columns
     * @return static (fluent function)
     */
    public function withCols($cols)
    {
        $this->cols = $cols;
        return $this;
    }

    /**
     * Set the number of columns for the text area
     * @param int $rows The number of rows
     * @return static (fluent function)
     */
    public function withRows($rows)
    {
        $this->rows = $rows;
        return $this;
    }

    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $html = '<textarea ' .
        'class="' . join(' ', $this->inputClass) . '" ' .
        'placeholder="' . $this->getPlaceHolder() . '" ' .
        'name="' . $this->name . '" ' .
        'id="' . $this->id . '" ' .
        'cols="' . $this->cols . '" ' .
        'rows="' . $this->rows . '" ' .
        $this->getAttributes() .
        $this->disabled .
        '>' .
        e($this->getDefaultValue()) .
        '</textarea>';
        if($this->hasAttribute('data-max-characters')){
            $html .= '<p class="characters-left"><span>' . $this->getAttribute('data-max-characters') . '</span>
                            karakters resterend</p>';
        }
        return $html;
    }
}
