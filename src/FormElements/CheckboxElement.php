<?php namespace Jcgroep\BuildIt\FormElements;

use Jcgroep\BuildIt\FormElements\Text\HiddenInput;
use Jcgroep\BuildIt\FormElements\Text\InputElement;
use Request;

/**
 * Class CheckboxElement
 * @package App\Html\FormElements
 */
class CheckboxElement extends InputElement
{
    protected $inline = false;

    /**
     * Set the type of the input to checkbox
     */
    public function __construct()
    {
        $this->forType('checkbox');
        $this->withClasses(['xs-top-margin', 'col-md-1', 'col-xs-1', 'col-sm-1']);
    }

    /**
     * Check the old input and the default value whether this attribute is checked or not
     * @return string checked when the default value is true, else an empty string
     */
    public function getDefaultValue()
    {
        if (Request::old($this->name) !== null) {
            return Request::old($this->name) == 'on' ? ' checked' : '';
        }

        return parent::getDefaultValue() == 1 ? ' checked' : '';
    }

    public function dependsOn(FormElement $element, $value)
    {
        $this->dependingOn = ['element' => $element, 'value' => $value];
        return $this;
    }

    public function inline()
    {
        $this->inline = true;
        $this->postfix = $this->label;
        $this->label = "&nbsp;";

        return $this->withClasses(['col-md-8 col-xs-8 col-sm-8 text-input pt-2']);
    }

    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $postfix = "";
        if ($this->inline) {
            $postfix = $this->postfix;
            $this->postfix = "";
        }
        if($this->isDisabled()){
            if($this->getDefaultValue() == ' checked'){
                return '<span class="fa fa-check-square-o" style="margin-right: 5px"></span>' . $postfix;
            }else{
                return '<span class="fa fa-close" style="margin-right: 5px"></span>' . $postfix;
            }
        }
        $hiddenInput = HiddenInput::create()
            ->withName($this->name)
            ->withDefaultValue('0');
        return $hiddenInput->render() . '<input ' .
        'class="' . join(' ', $this->inputClass) . '" ' .
        'id="' . $this->name . '" ' .
        'name="' . $this->name . '" ' .
        'type="' . $this->type . '" ' .
        'value="1" ' .
        ($this->required ? ' required' : '') .
        $this->getDefaultValue() .
        $this->disabled .
        '>' . $postfix;
    }
}
