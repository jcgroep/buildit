<?php namespace Jcgroep\BuildIt\FormElements;

use App\Models\Utils\ValueObjects\Files\MimeTypeSupport;
use Jcgroep\BuildIt\FormElements\Text\InputElement;

class FileInputElement extends InputElement
{
    public function __construct()
    {
        $this->forType('file');
        $this->withClasses(['col-md-5', 'col-xs-5', 'col-sm-5', 'text-input']);
        $this->optionalAttributes['accept'] = '*';
    }

    public function allow($type)
    {
        if ($this->optionalAttributes['accept'] == '*') {
            $this->optionalAttributes['accept'] = $type;
        } else {
            $this->optionalAttributes['accept'] = $this->optionalAttributes['accept'] . ',' . $type;
        }
        return $this;
    }

    public function allowVideo()
    {
        return $this->allow('video/*');

    }

    public function allowImage()
    {
        return $this->allow('image/*');
    }

    public function allowAudio()
    {
        return $this->allow('audio/*');
    }

    public function allowPdf()
    {
        foreach (MimeTypeSupport::pdf() as $mimeType) {
            $this->allow($mimeType);
        }
        return $this;
    }

    public function allowOfficeFiles()
    {
        foreach (MimeTypeSupport::officeFiles() as $mimeType) {
            $this->allow($mimeType);
        }
        return $this;
    }

    public function renderElement()
    {
        $html = parent::renderElement();
        $html .= '<progress class="progress-bar" value="0" max="100" style="width: 100%; height: 20px; display: none"></progress>';
        $html .= '<script></script>';
        return $html;
    }
}