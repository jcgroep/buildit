<?php namespace Jcgroep\BuildIt\FormElements\Controls;

use Lang;

class DropdownButtonGroup extends ButtonGroup
{
    public function render()
    {
        $html = '<div class="btn-group pull-right caption">';
        $html .= '<a href="#" class="btn blue dropdown-toggle" id="toggle-exercise-actions" data-toggle="dropdown">';
        $html .= trans('BuildIt::global.Actions') . ' <i class="fa fa-angle-down"></i>';
        $html .= '</a>';
        $html .= '<div class="dropdown-menu pull-right dropdown-custom">';
        $html .= '<ul class="nav">';
        foreach ($this->buttons as $button) {
            /** @var $button Button */
            $html .= '<li>';
            $html .= $button->withId($this->baseId . '-' . $button->getType())
                ->withClass('');
            $html .= '</li>';
        }
        $html .= '</ul>';
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}