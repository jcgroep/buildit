<?php namespace Jcgroep\BuildIt\FormElements\Controls;

class CancelButton extends Button
{
    protected $type = 'cancel';

    public function __construct()
    {
        $this->withAttribute('class', 'btn default');
        $this->withAttribute('type', 'button');
        $this->withDataAttribute('dismiss', 'modal');
    }

    public function render()
    {
        return '<button ' . $this->getAttributes() . ' >'
            . ($this->title ?? trans('BuildIt::global.Cancel'))
            . '</button>';
    }
}