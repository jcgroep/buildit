<?php namespace Jcgroep\BuildIt\FormElements\Controls;

use Jcgroep\BuildIt\FormElements\Text\HiddenInput;

class SecondaryButton extends Button
{
    public function __construct()
    {
        $this->withAttribute('class', 'btn btn-outline-primary');
        $this->withAttribute('type', 'submit');
    }

    public function render()
    {
        $this->withAttribute('value', $this->title ?? trans('BuildIt::global.Save'));
        return HiddenInput::create()->withName('secondary_action')->withId('secondary_action')->render() . '<input ' . $this->getAttributes() . ' onClick=\'$("input[name=secondary_action]").val(1);\' style="margin-left: 10px;"/>';
    }
}