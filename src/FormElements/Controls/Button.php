<?php namespace Jcgroep\BuildIt\FormElements\Controls;


use Jcgroep\BuildIt\Traits\HtmlTagTrait;

abstract class Button
{
    use HtmlTagTrait;

    protected $id;
    protected $link;
    protected $function;
    protected $title;
    protected $type = 'submit';

    public static function make()
    {
        return new static();
    }

    public function getType()
    {
        return $this->type;
    }

    public function withClass($class)
    {
        $this->withAttribute('class', $class);
        return $this;
    }

    public function withTitle($name)
    {
        $this->title = $name;
        return $this;
    }

    public function withId($id)
    {
        $this->withAttribute('id', $id);
        return $this;
    }

    public function withHotkey($hotkey)
    {
        $this->withDataAttribute('hotkey', $hotkey);
        return $this;
    }

    public abstract function render();

    public function __toString()
    {
        return $this->render();
    }
}