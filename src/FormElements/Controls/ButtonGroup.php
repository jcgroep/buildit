<?php namespace Jcgroep\BuildIt\FormElements\Controls;

use Illuminate\Database\Eloquent\Collection;

class ButtonGroup
{
    /**
     * @var Collection
     */
    protected $buttons;
    protected $baseId;

    public static function make($buttons = [])
    {
        return new static($buttons);
    }

    public function __construct($buttons = [])
    {
        $this->buttons = Collection::make($buttons)
            ->reject( function ($button) {
                return $button == null;
            });
    }

    public function withButton (Button $button)
    {
        $this->buttons->push($button);
        return $this;
    }

    public function withBaseId($id)
    {
        $this->baseId = $id;
        return $this;
    }

    public function getButtons()
    {
        return $this->buttons;
    }

    public function canSubmit()
    {
        foreach($this->buttons as $button){
            if($button instanceof SubmitButton){
                return true;
            }
        }
        return false;
    }

    public function render()
    {
        $html = '<span class="actions">';
        foreach($this->buttons as $button){
            $html .= $button->withId($this->baseId . '-'. $button->getType());
        }
        return $html . '</span>';
    }

    public function __toString()
    {
        return $this->render();
    }
}