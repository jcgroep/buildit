<?php namespace Jcgroep\BuildIt\FormElements\Controls;

use Form;
use Lang;

class DeleteButton extends Button
{

    protected $route;
    protected $deleteId;

    public function __construct()
    {
        $this->withAttribute('class', 'btn btn-danger');
        $this->withAttribute('type', 'submit');
        $this->withDataAttribute('confirm', trans('global.AreYouSure'));
    }

    public function withRoute($route)
    {
        $this->route = $route;
        return $this;
    }

    public function withDeleteId($deleteId)
    {
        $this->deleteId = $deleteId;
        return $this;
    }

    public function render()
    {
        $html = Form::open(['route' => [$this->route, $this->deleteId], 'method' => 'delete', 'data-remote']);
        $html .= '<button ' . $this->getAttributes() . '">';
        $html .= '<i class="fa fa-trash"></i>&nbsp; ' . trans('actions.Delete');
        $html .= '</button>';
        $html .= Form::close();
        return $html;
    }
}