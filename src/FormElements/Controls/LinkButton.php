<?php namespace Jcgroep\BuildIt\FormElements\Controls;

class LinkButton extends  Button
{
    protected $type = 'link';

    public function __construct()
    {
        $this->withAttribute('class', 'btn blue');
        $this->withAttribute('href', '#');
    }

    public function withLink($link)
    {
        $this->withAttribute('href', $link);
        return $this;
    }

    public function render()
    {
        return '<a ' . $this->getAttributes() . '>'
            . ($this->title ?? trans('BuildIt::global.edit'))
            . '</a>';
    }
}