<?php namespace Jcgroep\BuildIt\FormElements\Controls;

class SubmitButton extends Button
{
    public function __construct()
    {
        $this->withAttribute('class', 'btn blue');
        $this->withAttribute('type', 'submit');
    }

    public function render()
    {
        $this->withAttribute('value', $this->title ?? trans('BuildIt::global.Save'));
        return '<input ' . $this->getAttributes() . ' />';
    }
}