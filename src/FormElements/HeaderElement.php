<?php namespace Jcgroep\BuildIt\FormElements;

use Jcgroep\BuildIt\FormElements\Text\InputElement;
use Illuminate\Support\MessageBag;

/**
 * Class HeaderElement
 * @package App\Html\FormElements
 */
class HeaderElement extends InputElement
{
    public function render(MessageBag $errors = null)
    {
        $html = '<div id="' . $this->name . '" class="form-group">';
        $html .= '<h3>' . $this->label . '</h3>';
        $html .= '</div>';
        $html .= '<script>' . $this->getJavascript() . '</script>';
        return $html;
    }

    public function getJsSelector()
    {
        return '#' . $this->getId() . '';
    }
}
