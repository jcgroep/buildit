<?php namespace Jcgroep\BuildIt\FormElements;

use Illuminate\Support\Str;
use Jcgroep\BuildIt\FormElements\Text\InputElement;
use Illuminate\Support\MessageBag;

/**
 * Class HeaderElement
 * @package App\Html\FormElements
 */
class NoteElement extends InputElement
{
    public function __construct()
    {
        $this->labelClass = "note note-info";
    }

    public function render(MessageBag $errors = null)
    {
        $label = Str::contains($this->label, "<") ? $this->label : nl2br($this->label);
        $html = '<div id="' . $this->name . '" class="form-group">';

        $html .= '<div class="' . $this->labelClass . '">
                    <p>' . $label. '</p>
                 </div>';
        $html .= "</div>";
        $html .= '<script>' . $this->getJavascript() . '</script>';

        return $html;
    }

    public function getJsSelector()
    {
        return '#' . $this->getId() . '';
    }
}
