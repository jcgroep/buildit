<?php namespace Jcgroep\BuildIt\FormElements;

/**
 * Class SimpleDropdownElement
 * @package App\Html\FormElements
 */
class SimpleDropdownElement extends FormElement
{

    /**
     * @var Array containing the value and names showed by the dropdown
     */
    protected $options = [];

    /**
     * @var String id of the select element
     */
    protected $id;

    /**
     * @var String of dropdown empty state, to reset selection
     */
    protected $resetSelectionText;

    /**
     * Set the collection of objects to be dropped down
     * @param Array $options with key as select value and value as select name
     * @return static (fluent function)
     */
    public function withOptions(Array $options)
    {
        $this->options = $options + $this->options;
        return $this;
    }

    public function withEmptyValue(): SimpleDropdownElement
    {
        $this->options = ['' => ''] + $this->options;
        return $this;
    }

    /**
     * Set the id of the select element
     * @param String $id
     * @return static (fluent function)
     */
    public function withId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Shows empty state to reset selection with text
     * @param String $resetSelectionText showing such state with text
     * @return static (fluent function)
     */
    public function withResetSelectiontext($resetSelectionText)
    {
        $this->resetSelectionText = $resetSelectionText;
        return $this;
    }

    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $html = '<select ';
        if (isset($this->id)) {
            $html .= 'id="' . $this->id . '" ';
        }
        if (isset($this->jsonData) && is_string($this->jsonData)) {
            $html .= "data-json='" . $this->jsonData . "' ";
        }
        $html .= 'name="' . $this->name . '" ' .
        'class="' . 'form-control input-medium ' . join(' ', $this->classes) . $this->disabled . '">';


        if (isset($this->resetSelectionText)) {
            $html .= '<option value="" disabled="" selected="" style="display:none;">' . $this->resetSelectionText . '</option>';
        }
        foreach ($this->options as $value => $name) {
            $html .= SelectOption::create()
                ->withName($name)
                ->withValue($value)
                ->withDefaultValue($this->getDefaultValue())
                ->renderElement();
        }
        $html .= '</select>';

        if(isset($this->postfix)){
            $html .= "&nbsp; " . $this->postfix;
        }
        
        return $html;
    }
}
