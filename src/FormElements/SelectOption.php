<?php namespace Jcgroep\BuildIt\FormElements;


/**
 * Class SelectOption
 * @package App\Html\FormElements
 */
class SelectOption
{

    /**
     * @var mixed as the select value
     */
    protected $value;

    /**
     * @var String name
     */
    protected $name;

    /**
     * @var String to compare $value on, for if this option must be selected
     */
    protected $defaultValue;

    /**
     * Create a new instance of the option
     * @return static
     */
    public static function create()
    {
        return new static;
    }

    /**
     * Set the name of the  element
     * @param string $name The name of the option
     * @return static (fluid function)
     */
    public function withName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Set the name of the  element
     * @param mixed $value The value of the option
     * @return static (fluid function)
     */
    public function withValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Set the default value of the selections
     * @param mixed $defaultValue The default value, if is this options $value then we add the selected attribute
     * @return static (fluid function)
     */
    public function withDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
        return $this;
    }

    /**
     * Render this element
     * @return string the html of this option
     */
    public function renderElement()
    {
        $html = '<option value="' . $this->value . '"';
        if ((string)$this->defaultValue == (string)$this->value) {
            $html .= ' selected';
        }
        $html .= '>';
        $html .=  e($this->name) . '</option>';

        if(isset($this->postfix)){
            $html .= "&nbsp; " . $this->postfix;
        }
        return $html;
    }
}
