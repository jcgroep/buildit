<?php namespace Jcgroep\BuildIt\FormElements;

use Illuminate\Support\Collection;
use Request;
use Lang;

/**
 * Class DefaultDropdownElement
 * @package App\Html\FormElements
 */
class DefaultDropdownElement extends FormElement
{

    /**
     * @var Collection containing the objects showed by the dropdown
     */
    protected $collection;

    /**
     * @var String id of the select element
     */
    protected $selectId;

    /**
     * @var mixed default value
     */
    protected $defaultValue;

    /**
     * @var String of dropdown has a emty state, to reset selection
     */
    protected $resetSelectionText;
    protected $showItem = false;
    protected $showEmptyValue;
    protected $array;
    protected $translationKey;

    /**
     * Set the collection of objects to be dropped down
     * @param Collection $collection of eloquent objects
     * @param string $showItem The item to show in the dropdown
     * @return static
     */
    public function withCollection(Collection $collection, $showItem = 'name')
    {
        $this->array = [];
        $this->collection = $collection;
        $this->showItem = $showItem;
        return $this;
    }

    public function withArray($array)
    {
        $this->collection = new Collection();
        $this->array = $array;
        return $this;
    }

    public function withEmptyValue()
    {
        $this->showEmptyValue = true;
        return $this;
    }

    /**
     * Set the id of the select element
     * @param String $id
     * @return static (fluent function)
     */
    public function withId($id)
    {
        $this->selectId = $id;
        return $this;
    }

    /**
     * Shows empty state to reset selection with text
     * @param String $resetSelectionText showing such state with text
     * @return static (fluent function)
     */
    public function withResetSelectiontext($resetSelectionText)
    {
        $this->resetSelectionText = $resetSelectionText;
        return $this;
    }

    public function translateOptions($translationKey){
        $this->translationKey = $translationKey;
        return $this;
    }

    /**
     * Render this element
     * @return string the html of this input element
     */
    public function renderElement()
    {
        $this->collection->sortBy($this->showItem);

        $html = '<select ' .
            (isset($this->selectId) ? 'id="' . $this->selectId . '" ' : '') .
        'name="' . $this->name . '" ' .
        'class="' . 'form-control ' . (isset($this->inputSize) ? $this->inputSize : 'input-medium') . $this->disabled . '"' .
            $this->getAttributes() .
            '>';

        if (!$this->showEmptyValue && isset($this->resetSelectionText)) {
            $html .= '<option value="" disabled="" selected="" style="display:none;">' . $this->resetSelectionText . '</option>';
        }elseif($this->showEmptyValue){
            $html .= '<option value="" selected>' . (isset($this->resetSelectionText) ? $this->resetSelectionText : '') . '</option>';

        }
        foreach ($this->collection as $object) {
            $html .= SelectOption::create()
                ->withName(isset($this->translationKey) ? trans($this->translationKey . '.' . $object[$this->showItem]) : $object[$this->showItem])
                ->withValue($object->id)
                ->withDefaultValue($this->getDefaultValue())
                ->renderElement();
        }

        foreach($this->array as $key => $value){
            $html .= SelectOption::create()
                ->withName($value)
                ->withValue($key)
                ->withDefaultValue($this->getDefaultValue())
                ->renderElement();
        }

        $html .= '</select>';

        if(isset($this->postfix)){
            $html .= "&nbsp; " . $this->postfix;
        }
        return $html;
    }

    public function getJsSelector()
    {
        return '#' . $this->getId() . ' select';
    }
}
