<?php namespace Jcgroep\BuildIt\FormElements;

use Illuminate\Support\MessageBag;
use Jcgroep\BuildIt\Form;

class SubForm extends FormElement
{
    protected $elements = [];
    protected $title;
    protected $defaultClasses;
    protected $target;
    protected $classes = ['col-md-6', 'col-xs-12', 'col-sm-12'];


    public function inForm(Form $form)
    {
        $this->form = $form;
        if ($this->target == null) {
            $this->target = $form->getTarget();
        }
        return $this;
    }

    public function withTarget($target)
    {
        $this->target = $target;
        return $this;
    }

    public function addElement(FormElement $element)
    {
        $this->elements[] = $element;
        return $this;
    }

    public function addElements(array $elements)
    {
        $this->elements = array_merge($this->elements, $elements);
        return $this;
    }

    public function getElements() {
        return $this->elements;
    }

    public function getElement($name) {
        return collect($this->elements)->filter( function(FormElement $element) use ($name) {
            return $element->getName() == $name;
        })->first();
    }

    public function removeElement($name) {
        $this->elements = collect($this->elements)->reject( function(FormElement $element) use ($name) {
            return $element->getName() == $name;
        })->toArray();
    }

    public function withTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function getTitle() {
        return $this->title;
    }

    public function withDefaultClasses(array $classes)
    {
        $this->defaultClasses = $classes;
        return $this;
    }

    public function withDefaultLabelClass($classes)
    {
        $this->labelClass = $classes;
        return $this;
    }

    public function render(MessageBag $errors = null)
    {
        $html = '<div class="' . join(' ', $this->classes) . ' sub-form" id="' . $this->getId() . '">';
        $html .= $this->renderElement();
        foreach ($this->elements as $element) {
            $element->inForm($this->form);
            if($this->disabled){
                $element->disable();
            }
            if (isset($this->defaultClasses)) {
                $element->withClasses($this->defaultClasses);
            }
            if (isset($this->labelClass) && !isset($element->labelClass)) {
                $element->withLabelClass($this->labelClass);
            }
            if ($element->hasToBeRendered()) {
                $html .= $element->render($errors);
            }
        }
        $html .= '</div>';
        $html .= '<script>' . $this->getJavascript() . '</script>';
        return $html;
    }

    /**
     * @return mixed
     */
    public function renderElement()
    {
        if (isset($this->title)) {
            return '<h4>' . $this->title . '</h4>';
        }
        return '';
    }

    public function getJsSelector()
    {
        return '#' . $this->getId();
    }
}