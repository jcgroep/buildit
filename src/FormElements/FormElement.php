<?php namespace Jcgroep\BuildIt\FormElements;

use Illuminate\Support\MessageBag;
use Request;
use Illuminate\Support\Arr;
use Jcgroep\BuildIt\Form;
use Jcgroep\BuildIt\Traits\HtmlTagTrait;
use Jcgroep\Utils\Collections\BaseCollection;

/**
 * Class FormElement
 * @package App\Html\FormElements
 */
abstract class FormElement
{
    static $number = 0;

    use HtmlTagTrait;
    /**
     * @var string The name of the input field
     */
    protected $name;
    /**
     * @var string The default value (check self whether this is an value or the value of the edited target)
     */
    protected $defaultValue;
    /**
     * @var string The label to draw
     */
    protected $label;
    /**
     * @var bool whether this input is required or not
     */
    protected $required = false;
    /**
     * @var bool whether this input has to be rendered or not
     */
    protected $toBeRendered = true;
    /**
     * @var array the classes of the input element
     */
    protected $classes = ['col-md-4', 'col-xs-4', 'col-sm-4', 'text-input'];
    /**
     * @var array The data attributes to give to the input element
     */
    /**
     * @var array The data to give to the input element as json encoded data attribute
     */
    protected $jsonData = [];
    /**
     * @var string Postfix value which need to be displayed after the element
     */
    protected $postfix;
    protected $dependingOn = [];
    protected $labelClass;
    protected $inputSize;
    protected $inputFirst;
    protected $help;
    protected $disabled = '';
    protected $javascript = '';
    protected $autosave = false;
    protected $id;
    /**
     * @var Form The form this element is rendered in
     */
    protected $form;
    protected $readOnly = false;

    /**
     * Create a new instance of the form element
     * @return static
     */
    public static function create()
    {
        return new static;
    }

    public function getId()
    {
        if (!isset($this->id)) {
            $this->id = 'form-row-' . self::$number;
            self::$number++;
        }
        return $this->id;
    }

    public function withRowId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Helper function for preventing errors when wanting to suppress the submission of autocommit buttons.
     * @return static
     */
    public function suppressSubmission()
    {
        return $this;
    }

    public function withAutosave()
    {
        $this->autosave = true;
        return $this;
    }

    public function disable()
    {
        $this->disabled = ' disabled ';
        return $this;
    }

    public function isDisabled()
    {
        return $this->disabled == ' disabled ';
    }

    public function getDisabledAttribute()
    {
        return $this->disabled;
    }

    public function readOnly()
    {
        $this->readOnly = true;
    }



    /**
     * Set the name of the input element
     * @param string $name The name of the input element
     * @return static (fluid function)
     */
    public function withName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function inputFirst()
    {
        $this->inputFirst = true;
        return $this;
    }

    /**
     * Set the default value of the input
     * @param mixed $defaultValue The default value, there already has to be checked whether the default value has to be drawn or the value of the target
     * @return static (fluid function)
     */
    public function withDefaultValue($defaultValue)
    {
        if (is_object($defaultValue) && enum_exists($defaultValue::class)) {
            $this->defaultValue = $defaultValue->value;
        } else {
            $this->defaultValue = $defaultValue;
        }
        return $this;
    }

    /**
     * Set the label of the input
     * @param string $label The label of this form element
     * @return static (fluid function)
     */
    public function withLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function withHelp($helpText)
    {
        $this->help = $helpText;
        return $this;
    }

    /**
     * Draw this element only when this equation is true
     * @param bool $equation
     * @return static (fluid function)
     */
    public function onlyWhen($equation)
    {
        if (!$equation) {
            $this->toBeRendered = false;
        }
        return $this;
    }

    /**
     * Getter for checking whether this element has to be rendered or not
     * @return bool
     */
    public function hasToBeRendered()
    {
        return $this->toBeRendered;
    }

    /**
     * This form element is required
     * @return static (fluid function)
     */
    public function isRequired($required = true)
    {
        $this->required = $required;
        return $this;
    }

    public function getRequiredAttribute()
    {
        return $this->required ? 'required ' : '';
    }

    /**
     * Set the classes of the input field
     * @param array $classes array of string which are the default classes
     * @return static (fluid function)
     */
    public function withClasses(array $classes)
    {
        $this->classes = $classes;
        return $this;
    }

    public function withJavascript($script)
    {
        $this->javascript = $script;
        return $this;
    }

    /**
     * Add json encoded data as a data attribute to the element
     * @param $data Mixed data
     * @return $this
     */
    public function withJsonData($data)
    {
        $this->jsonData = json_encode($data);
        return $this;
    }

    /**
     * Add a postfix value
     * @param string $postfix
     * @return static (fluid function)
     */
    public function withPostfix($postfix)
    {
        $this->postfix = $postfix;
        return $this;
    }

    public function dependsOnMany(array $dependingOn)
    {
        $this->dependingOn = $dependingOn;
        return $this;
    }

    /**
     * This element depends on whether another element has a specific value
     * @param FormElement $element The element to check
     * @param mixed $value The value the element has
     * @return static (fluid function)
     */
    public function dependsOn(FormElement $element, $value)
    {
        $this->dependingOn[] = ['element' => $element, 'value' => '\'' . $value . '\''];
        return $this;
    }

    public function withLabelClass($labelClass)
    {
        $this->labelClass = $labelClass;
        return $this;
    }

    public function withInputSize($inputSize)
    {
        $this->inputSize = $inputSize;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPostfix() {
        return $this->postfix;
    }

    public function getLabel() {
        return $this->label;
    }

    /**
     * Check the default value and the old input which value has to be default.
     * @return string
     */
    public function getDefaultValue()
    {
        if (isset($this->name) && !empty(Request::old($this->name))) {
            return Request::old($this->name);
        }
        if (!isset($this->defaultValue) && isset($this->form) && !empty($this->form->getTarget())) {
            return $this->form->getTarget()->getAttribute($this->name);
        }
        return $this->defaultValue;
    }

    protected function hasError(MessageBag $errors = null)
    {
        if (isset($errors)) {
            if ($errors->has($this->name)) {
                return true;
            }
            return $errors->has($this->name . 'Hash');
        }
        return false;
    }

    protected function getErrorMessage(MessageBag $errors = null)
    {
        if ($this->hasError($errors)) {
            if ($errors->has($this->name)) {
                return $errors->first($this->name);
            } else {
                return str_replace('hash ', '', $errors->first($this->name . 'Hash'));
            }
        }
        return '';
    }

    /**
     * Render this element
     * @param MessageBag $errors
     * @return string the html of this input element
     */
    public function render(MessageBag $errors = null)
    {
        $html = '';
        if (!empty($this->label)) {
            $html .= '<div id="' . $this->getId() . '" class="form-group ' . ($this->required ? ' required' : '') . ($this->hasError($errors) ? ' has-error' : '') . '">';
        }
        if (!$this->inputFirst) {
            $html .= $this->getLabelHtml();
        }
        $html .= '<div class="' . join(' ', $this->classes) . '">';
        if ($this->readOnly) {
            $html .= '<span class="read-only-value">' . $this->getDefaultValue() . '</span>';
        } else {
            $html .= $this->renderElement();
        }
        $html .= '<script>' . $this->javascript . '</script>';
        $html .= '</div>';

        if ($this->hasError($errors)) {
            $html .= '<span class="error help-inline help-block">' . $this->getErrorMessage($errors) . '</span>';
        }
        if (isset($this->element)) {
            $html .= $this->element->render();
        }
        if ($this->inputFirst) {
            $html .= $this->getLabelHtml();
        }
        if (!empty($this->label)) {
            $html .= '</div>';
        }
        $html .= '<script>' . $this->getJavascript() . '</script>';
        return $html;
    }

    public function getLabelHtml()
    {
        if (empty($this->label)) {
            return '';
        }
        $html = '<label for="' . $this->name . '" class="' . (isset($this->labelClass) ? $this->labelClass : 'col-md-3 col-xs-3 col-sm-3 control-label') . '">';
        $html .= $this->label;
        if ($this->autosave) {
            $html .= view('alerts.ajaxAutosave')->render();
        }
        $html .= '</label>';
        if (isset($this->help)) {
            $html .= '<i class="fa blue-col fa-question-circle tooltips help-icon" data-placement="left" data-original-title="' . $this->help . '">&nbsp;</i>';
        }
        return $html;
    }

    protected function getJsValueFunction($setValue = null)
    {
        if ($setValue === null) {
            return 'val()';
        }
        return 'val("' . $setValue . '")';
    }

    public function getJavascript()
    {
        $js = '';
        $js .= 'if(answers == undefined) { var answers = {};}';
        foreach ($this->dependingOn as $dependency) {
            $js .= "$(document).on('change init', '" . $dependency['element']->getJsSelector() . "', function(e){";
            $js .= '    var formId = $(this).parents("form").attr("id");';
            $js .= '    if(!(formId in answers) || e.handleObj.type == "change") {';
            $js .= '        answers[formId] = {};';
            $js .= '        var disabledElements = $(this).parents("form").find(":disabled").removeAttr("disabled");';
            $js .= '        var tmpAnswers = $(this).parents("form").serializeArray();';
            $js .= '        disabledElements.attr("disabled","disabled");';
            $js .= '        tmpAnswers.forEach(function (answer) {';
            $js .= '            if(answer.name.match("^\[\]")){';
            $js .= '                name = answer.name.replace("[]", "");';
            $js .= '                answers[formId][name] = (name in answers[formId]) ? answers[formId][name] + "," + answer.value : answers[formId][name] = answer.value;';
            $js .= '            }else{';
            $js .= '                answers[formId][answer.name] = answer.value;';
            $js .= '            }';
            $js .= '        });';
            $js .= '    }';
            $js .= "    if(" . $this->isVisibleJs($this->dependingOn) . "){";
            $js .= "        $('#" . $this->getId() . "').show();";
            if ($this->required) {
                $js .= "    $('#" . $this->getId() . " input').prop('required',true)";
            }
            $js .= "    }else {";
            $js .= "        $('#" . $this->getId() . "').hide();";
            $js .= "        if( e.handleObj.type == \"change\" ) {";
            $js .= "            $('" . $this->getJsSelector() . "')." . $this->getJsValueFunction('') . ".trigger(e.handleObj.type);";
            $js .= "        }";
            if ($this->required) {
                $js .= "    $('#" . $this->getId() . " input').removeAttr('required')";
            }
            $js .= "    }";
            $js .= '});';
            //The timeout below is because rendering hidden items are rendered wrong...
            $js .= 'setTimeout(function(){';
            $js .= '    $(\'' . $dependency['element']->getJsSelector() . '\').trigger(\'init\');';
            $js .= '}, 100);';
        }

        return $js;
    }

    protected function isVisibleJs($dependingOn)
    {
        $js = '';
        foreach ($dependingOn as $index => $dependency) {
            if(!empty($dependency['element']->dependingOn)) {
                $js .= $this->isVisibleJs($dependency['element']->dependingOn) .' && ';
            }
            $js .= BaseCollection::make(explode('|', $dependency['value']))
                ->map(function ($value) use ($dependency) {
                    return $dependency['element']->getVisibilityJs($value, Arr::get($dependency, 'operator', '='));
                })->join('||');

            if ($index != sizeof($this->dependingOn) - 1) {
                $js .= ' && ';
            }
        }
        return $js;
    }

    public function inForm(Form $form)
    {
        $this->form = $form;
        return $this;
    }

    protected function getVisibilityJs($value, $operator)
    {
        $js = 'answers[formId]["' . $this->name . '"]';
        //Depending on

        $js .= ' ' . $this->getComparator($operator) . ' ';

        //The required value
        return $js . '"' . trim($value, '\'"') . '"';
    }

    protected function getComparator($operator)
    {
        if (class_exists('Jcgroep\Askit\DependencyOperator')) {
            return (new \Jcgroep\Askit\DependencyOperator($operator))->getCompareValue();
        } else {
            return $operator == '=' ? '==' : $operator;
        }
    }

    public function getJsSelector()
    {
        return '#' . $this->getId() . ' input';
    }

    /**
     * @return mixed
     */
    public abstract function renderElement();

    public function __toString()
    {
        return $this->render();
    }

    public function getDependingOn() {
        return $this->dependingOn;
    }

    public function withLivewireLazyModel($name)
    {
        return $this->withAttribute("wire:model.lazy", $name);
    }

    public function withLivewireModel($name)
    {
        return $this->withAttribute("wire:model", $name);
    }
}
